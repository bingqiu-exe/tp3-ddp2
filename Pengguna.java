import java.util.ArrayList;

public class Pengguna {
    private String name;
    private String bdate;
    private String address;
    private ArrayList<Course> activeCourses = new ArrayList<>();

    public Pengguna(String name, String bdate, String address) {
        this.name = name;
        this.bdate = bdate;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public String getBirthDate() {
        return bdate;
    }

    public String getAddress() {
        return address;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBirthDate(String bdate) {
        this.bdate = bdate;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void enrollCourse(Course course) {
        activeCourses.add(course);
    }

    public ArrayList<Course> getActiveCourses() {
        return activeCourses;
    }
}
