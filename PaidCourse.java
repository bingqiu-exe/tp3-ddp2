public class PaidCourse extends Course {
    public int coursePrice;

    public PaidCourse(String courseName, int coursePrice, Instruktur instructor) {
        super(courseName, instructor);
        this.coursePrice = coursePrice;
    }

    // Additional methods or attributes specific to PaidCourse, if needed
}
