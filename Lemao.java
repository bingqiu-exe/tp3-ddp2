import java.util.ArrayList;
import java.util.Scanner;


public class Lemao {
    private ArrayList<Pengguna> penggunas = new ArrayList<>();
    private ArrayList<Course> courses = new ArrayList<>();
    // private ArrayList<Course> activeCourses = new ArrayList<>();
    private ArrayList<ReportCard> reports = new ArrayList<>();
    private Pengguna loginPengguna;

    public static void main(String[] args) {
        Lemao app = new Lemao();
        System.out.println("Welcome to Lemao! Enjoy your Learning");

        app.initData();

        Scanner scanner = new Scanner(System.in);
        int choice;
        do {
            System.out.println("Menu" +
                    "\n1. Login" +
                    "\n2. Daftar sebagai Murid" +
                    "\n3. Daftar sebagai Instruktur" +
                    "\n0. Keluar");
            System.out.print("Pilih menu: ");
            choice = app.getUserChoice(scanner);
            switch (choice) {
                case 1:
                    app.userLogin(scanner);
                    break;
                case 2:
                    app.studentRegistration(scanner);
                    break;
                case 3:
                    app.instructorRegistration(scanner);
                    break;
                case 0:
                    System.out.println("Sampai Jumpa!");
                    break;
                default:
                    System.out.println("Pilihan menu tidak valid.");
                    break;
            }
        } while (choice != 0);
        scanner.close();
    }

    public int getUserChoice(Scanner scanner) {
        while (true) {
            if (scanner.hasNextInt()) {
                return scanner.nextInt();
            } else {
                System.out.println("Input invalid.");
                scanner.next();
            }
        }
    }

    public void studentRegistration(Scanner scanner) {
        System.out.print("Nama: ");
        String name = scanner.next();
        System.out.print("Tanggal Lahir: ");
        String bdate = scanner.next();
        System.out.print("Alamat: ");
        String address = scanner.next();
        Murid murid = new Murid(name, bdate, address);
        penggunas.add(murid);
        System.out.println("Pendaftaran berhasil silahkan login kembali\r\n");
    }

    public void instructorRegistration(Scanner scanner) {
        System.out.print("Nama: ");
        String name = scanner.next();
        System.out.print("Tanggal Lahir: ");
        String bdate = scanner.next();
        System.out.print("Alamat: ");
        String address = scanner.next();
        Instruktur instruktur = new Instruktur(name, bdate, address, false);
        penggunas.add(instruktur);
        System.out.println("Pendaftaran berhasil silahkan login kembali\r\n");
    }

    public void adminMenu(Scanner scanner) {
        int choice;
        while (true) {
            System.out.println("--------------Admin " + loginPengguna.getName() + " Menu--------------" +
                    "\n1. Lihat Instruktur" +
                    "\n2. Verifikasi Instruktur" +
                    "\n3. Buat Training" +
                    "\n0. Logout");
            System.out.print("Pilih menu: ");
            choice = getUserChoice(scanner);
            switch (choice) {
                case 1:
                    displayInstruktur();
                    break;
                case 2:
                    verifyInstruktur(scanner);
                    break;
                case 0:
                    System.out.println("Logging out...");
                    return;
                default:
                    System.out.println("Pilihan menu tidak valid.");
            }
        }
    }

    public void displayInstruktur() {
        System.out.println("------- Daftar Instruktur -------");
        int i = 1;
        for (Pengguna pengguna : penggunas) {
            if (pengguna instanceof Instruktur) { // ngecek klo pengguna itu instance dari instruktur
                Instruktur instruktur = (Instruktur) pengguna; // tipe instruktur
                System.out.println("-------  " + i++ + "   -------");
                System.out.println("Nama : " + instruktur.getName());
                System.out.println("Tanggal Lahir : " + instruktur.getBirthDate());
                System.out.println("Alamat : " + instruktur.getAddress());
                System.out.println("Status : " + (instruktur.isCertified() ? "Verified" : "Not Verified"));

                System.out.println("List Course :");
                if (instruktur.getActiveCourses().isEmpty()) { //ngambil course yang aktif
                    System.out.println("Belum ada course");
                } else {
                    for (Course course : instruktur.getActiveCourses()) { //untuk course diambil dari course aktif instruktur
                        System.out.println("- " + course.getCourseName());
                    }
                }
                System.out.println("\n");
            }
        }
    }

    public void verifyInstruktur(Scanner scanner) {
        System.out.println("Berikut instruktur yang belum terverifikasi:");
        int i = 1;
        boolean foundUnverified = false;
        for (Pengguna pengguna : penggunas) {
            if (pengguna instanceof Instruktur && !((Instruktur) pengguna).isCertified()) { //klo penggunanya instance dari iini sama iscertified
                System.out.println("- " + i++ + ". " + pengguna.getName()); // get nama instruktur
                foundUnverified = true; //diverifikasi
            }
        }

        if (!foundUnverified) {
            System.out.println("Tidak ada instruktur yang perlu diverifikasi.");
            return;
        }

        System.out.print("Siapa yang akan anda verifikasi? ");
        String instrukturName = scanner.next();
        Instruktur instrukturToVerify = findInstruktur(instrukturName); // nyari instruktur yang udh diinput tadi

        if (instrukturToVerify != null) { //klo instrukturnya ada
            instrukturToVerify.setCertified(true);
            System.out.println("Instruktur " + instrukturToVerify.getName() + " berhasil diverifikasi.");
        } else {
            System.out.println("Instruktur dengan nama " + instrukturName + " tidak ditemukan.");
        }
    }

    public Instruktur findInstruktur(String name) {
        for (Pengguna pengguna : penggunas) {
            if (pengguna instanceof Instruktur && pengguna.getName().equals(name)) { // cek klo penggunanya instance dari instruktur terus ambil nama terus nanti disamain sama equals parameter name
                return (Instruktur) pengguna;
            }
        }
        return null;
    }

    public void muridMenu(Scanner scanner) {
        while (true) {
            System.out.println("--------------Murid " + loginPengguna.getName() + " Menu------------------");
            System.out.println("1. Enroll Course");
            System.out.println("2. Lihat Course Aktif Saya");
            System.out.println("3. Lihat Rapor");
            System.out.println("0. Logout");
            System.out.print("Pilih menu: ");
            int studentChoice = getUserChoice(scanner);

            switch (studentChoice) {
                case 1:
                    enrollCourse(scanner);
                    break;
                case 2:
                    viewActiveCourses();
                    break;
                case 3:
                    viewReports();
                    break;
                case 0:
                    System.out.println("Logging out...");
                    return;
                default:
                    System.out.println("Pilihan menu tidak valid.");
            }
        }
    }

    public void instrukturMenu(Scanner scanner) {
        while (true) {
            System.out.println("--------------Instruktur " + loginPengguna.getName() + " Menu------------------");
            System.out.println("1. Buat Course");
            System.out.println("2. Lihat Course Saya");
            System.out.println("3. Buat Rapor Murid");
            System.out.println("0. Logout");
            System.out.print("Pilih menu: ");
            int instrukturChoice = getUserChoice(scanner);

            switch (instrukturChoice) {
                case 1:
                    createCourse(scanner);
                    break;
                case 2:
                    viewMyCourses();
                    break;
                case 3:
                    createReport(scanner);
                    break;
                case 0:
                    System.out.println("Logging out...");
                    return;
                default:
                    System.out.println("Pilihan menu tidak valid.");
            }
        }
    }

    public void createCourse(Scanner scanner) {
        System.out.println("--------- Membuat Course ---------");
        System.out.println("Jenis Course");
        System.out.println("1. Course Gratis");
        System.out.println("2. Course Berbayar");
        System.out.print("Pilih jenis course: ");
        int courseType = getUserChoice(scanner);

        System.out.print("Nama Course: ");
        String courseName = scanner.next();

        if (courseType == 2) { //untuk course yang berbayar
            System.out.print("Harga Course: ");
            int coursePrice = scanner.nextInt();
            courses.add(new PaidCourse(courseName, coursePrice, (Instruktur) loginPengguna)); //nambahin course yang berbayar
            System.out.println("Course berbayar berhasil ditambahkan");
        } else { //untuk course yang ga berbayar
            courses.add(new Course(courseName, (Instruktur) loginPengguna)); //nambahin course yang ga berbayar
            System.out.println("Course gratis berhasil ditambahkan");
        }

    }


    public void viewMyCourses() {
        if (courses.isEmpty()) {
            System.out.println("Belum ada course yang dibuat.");
        } 
        else {
            int i = 1;
            for (Course course : courses) {
                System.out.println("-------  " + i++ + "   -------"); //untuk nunjukkin nomor course yang udh dibuat
                System.out.println("Nama Course : " + course.getCourseName());
                System.out.println("Nama Instruktur: " + course.getInstructor().getName());
                System.out.println("Jumlah Murid: " + course.getEnrolledStudents().size());
                System.out.println("List Murid:");
                
                if (course.getEnrolledStudents().isEmpty()) {
                    System.out.println("belum ada yang mengambil Course ini");
                } 
                else 
                    for (Pengguna student : course.getEnrolledStudents()) { //untuk ngambil student yang udh enroll coursenya
                        System.out.println("- " + student.getName());
                    }
                }
                System.out.println("\n");
            }
        }

    public void userLogin(Scanner scanner) {
        System.out.print("Masukkan nama (maks. 1 kata): ");
        String name = scanner.next();
        if (isSingleWord(name)) {
            Pengguna foundUser = searchPengguna(name);
            if (foundUser != null) {
                setLoginPengguna(foundUser);
                System.out.println("Login sukses: " + name);

                if (foundUser instanceof Murid) {
                    muridMenu(scanner);
                } else if (foundUser instanceof Instruktur) {
                    instrukturMenu(scanner);
                } else if (foundUser instanceof Admin) {
                    adminMenu(scanner);
                }
            } else {
                System.out.println("Pengguna tidak ditemukan.");
            }
        } else {
            System.out.println("Nama harus berupa satu kata (tanpa spasi).");
        }
    }

    public Pengguna getLoginPengguna() {
        return loginPengguna;
    }

    public void setLoginPengguna(Pengguna user) {
        loginPengguna = user;
    }

    private Pengguna searchPengguna(String name) {
        for (Pengguna pengguna : penggunas) {
            if (pengguna.getName().equals(name)) {
                return pengguna;
            }
        }
        return null;
    }

    private boolean isSingleWord(String name) {
        return !name.contains(" ");
    }

    public void initData() {
        this.penggunas.add(new Admin("Fikri", "12/11/2001", "Jalan kebahagiaan", false));
        Instruktur instruktur1 = new Instruktur("Aristo", "13/11/86", "Jalan sesama", false);
        Instruktur instruktur2 = new Instruktur("Mark", "03/01/90", "Jalan Bersama", false);
        
        this.penggunas.add(instruktur1);
        this.penggunas.add(instruktur2);
        this.penggunas.add(new Murid("Drews", "12/09/03", "Sesame Street"));
    }


    public void enrollCourse(Scanner scanner) {
        System.out.println("Berikut course yang ditawarkan pada Lemao:");
        for (int i = 0; i < courses.size(); i++) {
            Course course = courses.get(i);
            System.out.println((i + 1) + ". Nama Course: " + course.getCourseName() + " - Instruktur: " + course.getInstructor().getName());
        }

        System.out.print("Masukkan nomor course: ");
        int courseChoice = getUserChoice(scanner) - 1;

        if (courseChoice >= 0 && courseChoice < courses.size()) {
            Course selectedCourse = courses.get(courseChoice);
            if (!isEnrolled(selectedCourse, loginPengguna)) {
                ((Murid) loginPengguna).enrollCourse(selectedCourse);
                selectedCourse.enrollStudent(loginPengguna);
                System.out.println(selectedCourse.getCourseName() + " berhasil di enroll.");
            } else {
                System.out.println("Gagal Enroll : " + selectedCourse.getCourseName() + " sedang di enroll.");
            }
        } else {
            System.out.println("Nomor course tidak valid.");
        }
        // coursechoice untuk milih course yang udah dibuat oleh instruktur
        // selectedcourse untuk get coursechoice yang udah dipilih murid
        // kalo udah keenroll, murid berhasil di enroll
        // kalau gagal enroll keluar ""
    }

    public void viewActiveCourses() {
        System.out.println("—-------- Course Aktif Saat Ini —--------");
        for (Course course : loginPengguna.getActiveCourses()) { //buat get course yang aktif buat pengguna murid
            System.out.println("Nama Course: " + course.getCourseName() + " - Instruktur: " + course.getInstructor().getName());
        }
    }
    // untuk ngeliat course yang aktif

    private boolean isEnrolled(Course course, Pengguna user) {
        return user.getActiveCourses().contains(course);
    }
    //untuk course yang udah keenroll

    //penjelasan kode createReport(Scanner scanner):
    public void createReport(Scanner scanner) {
        System.out.print("Masukkan nomor course: ");
        int courseChoice = getUserChoice(scanner) - 1;
        
        if (courseChoice >= 0 && courseChoice < courses.size()) { // buat mastiin kalo courseChoicenya >= 0 sama kurang dari course yang tersedia 
            Course selectedCourse = courses.get(courseChoice); // buat ngambil courses di coursechoise, pake variable selectedChoice
    
            System.out.println("Berikut merupakan murid yang berada di kelas ini"); // buat input
            int i = 1;
            for (Pengguna student : selectedCourse.getEnrolledStudents()) { // untuk ngambil pengguna murid di selectedcourse
                System.out.println(i + ". " + student.getName()); // keluarannya jadi 1. Nasywa (contohnya)
                i++;
            }
    
            System.out.print("Masukkan nomor murid: "); // input
            int studentChoice = getUserChoice(scanner) - 1; 
    
            if (studentChoice >= 0 && studentChoice < selectedCourse.getEnrolledStudents().size()) {
                Pengguna selectedStudent = selectedCourse.getEnrolledStudents().get(studentChoice); // ngambil murid yang dipilih di selectedcourse yg udh dienroll
                System.out.print("Nilai untuk " + selectedStudent.getName() + " pada " + selectedCourse.getCourseName() + ": ");
                int score = getUserChoice(scanner); 

                int points = calculatePoints(score); 

                System.out.print("Feedback untuk murid: ");
                System.out.print("Report card berhasil ditambahkan, point saat ini: " + points);
                scanner.nextLine();
                String feedback = scanner.nextLine(); 
                
                ((Instruktur) loginPengguna).createReport((Murid) selectedStudent, selectedCourse, points, feedback);
            } else {
                System.out.println("Nomor murid tidak valid.");
            }
        } else {
            System.out.println("Nomor course tidak valid.");
        }
    }

    private int calculatePoints(int score) {
        if (score > 85){
            return 25;
        }
        else if (85 <= score && score > 70) {
            return 20;
        }
        else if (70 <= score && score > 55) {
            return 10;
        }
        else {
            return 5;
        }
    }

    public void viewReports() {
        System.out.println("---------- RAPOR ----------");
        System.out.println("Total Point Anda: " + calculatePoints(0));
        System.out.println("Detail:");

        for (ReportCard report : reports) {
            System.out.println("----------  " + (report.indexOf(report) + 1) + "  ----------");
            System.out.println("Nama Murid: " + report.getStudent().getName());
            System.out.println("Nama Course: " + report.getCourse().getCourseName());
            System.out.println("Nilai: " + report.getScore());
            System.out.println("Feedback: " + report.getFeedback());
        }
    }
}
