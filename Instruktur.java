public class Instruktur extends Pengguna {
    private boolean isCertified;

    public Instruktur(String name, String bdate, String address, boolean isCertified) {
        super(name, bdate, address);
        this.isCertified = isCertified;
    }

    public boolean isCertified() {
        return isCertified;
    }

    public void setCertified(boolean certified) {
        isCertified = certified;
    }

    public void createReport(Murid selectedStudent, Course selectedCourse, int points, String feedback) {
    }
}
