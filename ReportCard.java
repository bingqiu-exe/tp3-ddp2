public class ReportCard {
    public Murid student;
    public Course course;
    public int score;
    public int points;
    public String feedback;

    public ReportCard(Murid student, Course course, int score, int points, String feedback) {
        this.student = student;
        this.course = course;
        this.score = score;
        this.points = points;
        this.feedback = feedback;
    }

    public String getPoints() {
        return null;
    }

    public Course getCourse() {
        return null;
    }

    public Pengguna getStudent() {
        return null;
    }

    public int indexOf(ReportCard report) {
        return 0;
    }

    public String getFeedback() {
        return null;
    }

    public String getScore() {
        return null;
    }
}
