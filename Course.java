import java.util.ArrayList;

public class Course {
    private String courseName;
    private Instruktur instructor;
    private ArrayList<Pengguna> enrolledStudents;

    // Constructor for a free course
    public Course(String courseName2, Instruktur loginPengguna) {
        this.courseName = courseName2;
        this.instructor = loginPengguna;
        this.enrolledStudents = new ArrayList<>();
    }

    // Constructor for a paid course
    public Course(Instruktur instructor, String courseName, int coursePrice) {
        this.courseName = courseName;
        this.instructor = instructor;
        this.enrolledStudents = new ArrayList<>();
        // Additional logic for handling paid courses if needed
    }

    public String getCourseName() {
        return courseName;
    }

    public Instruktur getInstructor() {
        return instructor;
    }

    public ArrayList<Pengguna> getEnrolledStudents() {
        return enrolledStudents;
    }

    // Add a method to enroll a student in the course
    public void enrollStudent(Pengguna student) {
        enrolledStudents.add(student);
    }

    // Add a method to check if a student is already enrolled in the course
    public boolean isStudentEnrolled(Pengguna student) {
        return enrolledStudents.contains(student);
    }

    public int size() {
        return 0;
    }

    public Course get(int courseChoice) {
        return null;
    }
}
